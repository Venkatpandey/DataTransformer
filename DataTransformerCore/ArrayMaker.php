<?php
/**
 * User: venpan
 * Date: 17/11/2017
 * Time: 15:28
 */

namespace DataTransform\ArrayMaker;
use Symfony\Component\Yaml\Parser;

/**
 * Class ArrayMaker
 *
 * @package DataTransform\ArrayMaker
 */
class ArrayMaker
{
    /**
     * Helper function to convert Yaml data to array
     *
     * @param $file
     * @return mixed
     */
    public function YAMLToArray ($file)
    {
        $yaml = new Parser();
        $arrayFromYaml = $yaml->parse(file_get_contents($file));

        return $arrayFromYaml;
    }

    /**
     * Helper function to convert Json data to array
     *
     * @param $file
     * @return array
     */
    public function JSONToArray ($file)
    {
        $jsonStringData = file_get_contents($file);
        $arrayFromJson = json_decode(utf8_encode($jsonStringData));
        $error = json_last_error();

        return $arrayFromJson;
    }

    /**
     * Helper function to combine and return data and its corresponding model
     *
     * @param $Data
     * @param $ProcessedData
     * @return array
     */
    public function CombineModelData ($Data, $ProcessedData)
    {
        $Data_r['EntityModel'] = $Data['EntityModel'];
        $Data_r['EntityData'] = $ProcessedData;

        return $Data_r;
    }

    /**
     * Simple helper function convert XML to Associative Array
     *
     * @param $file
     * @return array
     */
    public function XMLToArray ($file)
    {
        $Xmldata = simplexml_load_file($file);
        $array = json_decode(json_encode((array) $Xmldata), 1);

        return $array;
    }

    /**
     * Simple helper function convert CSV to Associative Array
     *
     * @param $file
     * @return array
     */
    public function CSVToArray ($file)
    {
        $allRows = array();
        $header = null;
        // initiate temporary resource file
        $tempResourceFile = fopen($file, 'r');
        while ($row = fgetcsv($tempResourceFile)) { // get all the rows
            if ($header === null) {
                $header = $row;
                continue;
            }
            $allRows[] = array_combine($header, $row); // association with header
        }

        return $allRows;
    }

    /**
     * Call this method to get singleton
     *
     * @return ArrayMaker
     */
    public static function Maker()
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new ArrayMaker();
        }
        return $instance;
    }

    /**
     * Returns passed array with key re-indexed
     *
     * @param $Array
     * @return array
     */
    public function ReIndexArray($Array)
    {
        return array_values($Array);
    }

    /**
     * ArrayMaker constructor.
     */
    private function __construct()
    {
    }
}