<?php
/**
 * User: venpan
 * Date: 18/10/2017
 * Time: 19:45
 */

namespace MakeResultClass\ToXML;

include_once('ResultBase.php');
use ResultBase\ResultBase;
header('Content-type: text/plain');

/**
 * Class ToXML
 *
 * @package MakeResultClass\ToXML
 */
class ToXML extends ResultBase
{
    /**
     * Function to make XML data file
     * outputs in DataTransformer::TARGET_DIR
     *
     * @param $ObjectData
     * @param $Format
     */
    public function ToXML ($ObjectData, $Format)
    {
        // make array if required
        $Array = $this->prepareDataToWrite($ObjectData);

        $xmlData = new \SimpleXMLElement("<?xml version=\"1.0\"?><".$Format['basename']."></".$Format['basename'].">");
        $this->arrayToXML($Array, $xmlData);
        $status = $xmlData->asXML($this->getFileLocation()) ? true : false;
        $this->setResult($status);
    }

    /**
     * helper function to convert array to xml data
     *
     * @param $array
     * @param $xmlData
     */
    private function arrayToXML($array, &$xmlData)
    {
        // loop through each element and add to xmldata
        foreach($array as $key => $value) {
            if(is_array($value)) {
                if(!is_numeric($key)){
                    $subnode = $xmlData->addChild("$key");
                    $this->arrayToXML($value, $subnode);
                }else{
                    $subnode = $xmlData->addChild("hotel$key");
                    $this->arrayToXML($value, $subnode);
                }
            }else {
                $xmlData->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }

    /**
     * ToXML constructor.
     *
     */
    public function __construct()
    {

    }
}