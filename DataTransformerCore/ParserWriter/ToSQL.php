<?php
/**
 * User: venpan
 * Date: 17/11/2017
 * Time: 10:09
 */

namespace MakeResultClass\ToSQL;

include_once('ResultBase.php');
use ResultBase\ResultBase;
header('Content-type: text/plain');

/**
 * Class ToSQL
 *
 * @package MakeResultClass\ToSQL
 */
class ToSQL extends ResultBase
{
    /**
     * Function to make SQL insert file
     * outputs in DataTransformer::TARGET_DIR
     *
     * @param $ObjectData
     * @param $Format
     */
    public function ToSQL ($ObjectData, $Format)
    {
        // init sql file content
        $sqlContents = '';
        $Data_r = $this->prepareData($ObjectData);
        // loop through Object and apend to sql inserts
        foreach ($Data_r as $Id => $Data) {
            $sqlInsert = $this->fetchPreInsert($ObjectData['EntityModel'], $Format);
            $sqlvalue = $this->fetchValues($Data);
            $sqlContents .= $sqlInsert . $sqlvalue;
        }

        // write content to file
        $sqlFile= file_put_contents($this->getFileLocation(), $sqlContents);
        $sqlFile != null ? $this->setResult(true) : $this->setResult(false);
    }

    /**
     * Handles preparation of data to write
     *
     * @param $ObjectData
     * @return array
     */
    private function prepareData($ObjectData)
    {
        $dataMaped = $this->prepareDataToWrite($ObjectData);

        // sanitisation for string types
        foreach ($dataMaped as $id => $data) {
            $dataMaped_[$id] = $this->sanitizeThis($data);
        }

        return $dataMaped;
    }

    /**
     * For SQL inserts make sure to escape quotes by adding slashes
     *
     * @todo : find some better logic to check different datatypes.
     * @param $data
     * @return array
     */
    private function sanitizeThis($data)
    {
        $data_ = [];
        foreach ($data as $col => $value) {
            //if(gettype($value) == "string") {
                $data_[$col] = addslashes($value);
            //}
        }
        return $data_;
    }

    /**
     * Prepare SQL Inserts stements to be used by this class during wriring sql file.
     *
     * @param $Model
     * @param $Format
     * @return string
     */
    private function fetchPreInsert($Model, $Format)
    {
        $sqlInsert = "INSERT INTO `".$Format['basename']."` (";
        foreach ($Model as $col => $node) {
            $sqlInsert .= "`".array_search($node, $Model)."`, ";

        }
        $sqlInsert = rtrim($sqlInsert,", ");
        $end = ") VALUES" . PHP_EOL;

        return $sqlInsert . $end;
    }

    /**
     * Prepare stements to be used by this class during wriring sql file.
     *
     * @param $Data
     * @return string
     */
    private function fetchValues ($Data)
    {
        $sqlValues =  "(";
        foreach ($Data as $col => $value) {
            $sqlValues .= "`" . $value . "`, ";
        }
        $sqlValues = rtrim($sqlValues,", ");
        $end = ");\n" . PHP_EOL;

        return $sqlValues . $end;
    }

    /**
     * ToSQL constructor.
     */
    public function __construct()
    {

    }
}