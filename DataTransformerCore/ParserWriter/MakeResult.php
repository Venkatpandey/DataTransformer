<?php
/**
 * @author: Venkat Raman Pandey
 * Date: 18/10/2017
 * Time: 19:15
 */

namespace DataTransformer\MakeResultClass;
use DataTransform\ArrayMaker\ArrayMaker;
use DataTransform\DataTransformer;
use Exception;

include_once ('Delivery.php');
header('Content-type: text/plain');
/**
 * Class MakeResult
 * Responsible for making final result data for delivery
 *
 * @package MakeResultClass
 */
class MakeResultClass
{
    /** some constants to be used during process */

    const ALL_FORMAT = 'all';

    /**
     * Target format option map
     *
     * @var array
     */
    private $FormatSetting;

    /**
     * @var
     */
    private $ResultStatus;

    /**
     * @return mixed
     */
    public function getResultStatus()
    {
        return $this->ResultStatus;
    }

    /**
     * Helper function to make result file for output
     *
     * @param $ObjectData
     * @param $Format
     * @return bool
     */
    private function makeResultFile($ObjectData, $Format)
    {
        // load supported format class
        $this->includeSupportedFormats($Format);
        $ArrayUtils = ArrayMaker::Maker();
        $DataIdxed = $ArrayUtils->ReIndexArray($ObjectData['EntityData']);// reindex array
        $ObjectData = $ArrayUtils->CombineModelData($ObjectData, $DataIdxed);
        $status = false;
        // result file name
        $Resfilename = $Format['basename'] . "_resultData.";
        switch ($Format['target']) {
            default :
                $Data = $this->CallToCX($ObjectData, $Resfilename, $Format);
                if ($Data != null){
                    $status = $this->TryDeliver($Data, $Format);
                }
                    break;

            case self::ALL_FORMAT:
                $status = $this->CallToAll($ObjectData, $Resfilename, $Format);
                if ( $Format['download'] == "1"){
                    // special treatment for multiple files :)
                    $status = $this->Deliver(false, false);
                }
                break;
        }

        return $status;
    }

    /**
     * Helper function to check status and call Delivery class to initiate download
     *
     * @param $Data
     * @param $Format
     * @return bool
     */
    private function TryDeliver ($Data, $Format)
    {
        try{
            if ($Data->getResult() == true && $Format['download'] == "1") {
                $status = $this->Deliver($Data->getFileLocation(), true);
            } else {
                $status = $Data->getResult();
            }
        }catch (Exception $exception){
            echo "Error! Couldnt write output format file", $exception->getMessage(), "\n";
            $status = false;
        }

        return $status;
    }

    /**
     * For all format output, call all format class and delivery
     *
     * @param $ObjectData
     * @param $Resfilename
     * @param $Format
     * @return bool
     */
    private function CallToAll($ObjectData, $Resfilename, $Format)
    {
        foreach ($this->FormatSetting as $type => $format) {
            $Format['target'] = $format;
            $this->CallToCX($ObjectData, $Resfilename, $Format);
        }

        return true;
    }

    /**
     * Function gets target format, prepares class initiation and further processing
     *
     * @param $ObjectData
     * @param $Resfilename
     * @param $Format
     * @return $Data \MakeResultClass\format\format
     */
    private function CallToCX($ObjectData, $Resfilename, $Format)
    {
        if (in_array($Format['target'], $this->FormatSetting)) {
            // class initiation // arrange variable to call class as per incomming format
            $Class = "MakeResultClass" . "\\" . "To" . strtoupper($Format['target']) . "\\" . "To" . strtoupper($Format['target']);

            // try to initialize
            try {
                $Data = new $Class();
                $Data->setFileLocation(DataTransformer::LOCAL_DIR . $Resfilename . $Format['target']);
                $Data->{'To' . strtoupper($Format['target'])}($ObjectData, $Format);

            } catch (\Exception $ex) {
                echo 'Exception: ', $ex->getMessage(), "\n";
                $Data = null;
            }
        } else {
            echo "Format not supported..!\n";
            $Data = null;
        }

        return $Data;
    }

    /**
     * Helper function to include supported ParserWriter format classes
     *
     * @param $Format
     */
    private function includeSupportedFormats($Format)
    {
        $this->FormatSetting = $Format['Formats'];
        // stright forward
        try{
            if ($Format['target'] == self::ALL_FORMAT) {
                foreach ($this->FormatSetting as $key => $value) {
                    include_once('To' . strtoupper($value) . '.php');
                }
            } elseif (in_array($Format['target'], $this->FormatSetting)) {
                include_once('To' . strtoupper($Format['target']) . '.php');
            }
        } catch (Exception $exception) {
            echo "Format class not found.",  $exception->getMessage(), "\n";
        }
    }

    /**
     * @param $location
     * @param $isFile
     * @return bool
     */
    private function Deliver($location, $isFile)
    {
        // initiate the delivery class
        $Delivery = new \MakeResultClass\Delivery\Delivery();
        $status = $Delivery->Download($location, $isFile);

        return $status;
    }

    /**
     * MakeResultClass constructor.
     *
     * @param $Array
     * @param $Format
     */
    public function __construct($Array, $Format)
    {
        $Status = $this->makeResultFile($Array, $Format);
        $this->ResultStatus = $Status;
    }
}