<?php
/**
 * User: venpan
 * Date: 14/12/2017
 * Time: 11:25
 */

namespace ResultBase;

use DataTransform\DataObjects\DataObjects;

/**
 * Abstract Class ResultBase
 *
 * @package ResultBase
 * @abstract
 */
abstract class ResultBase extends DataObjects
{
    /**
     * Holder for current file location
     *
     * @var
     */
    private $FileLocation;

    /**
     * Format result variable
     *
     * @var
     */
    private $Result;

    /**
     * Getter for current file location
     *
     * @return mixed
     */
    public function getFileLocation()
    {
        return $this->FileLocation;
    }

    /**
     * Setter for current file location
     *
     * @param mixed $FileLocation
     */
    public function setFileLocation($FileLocation)
    {
        $this->FileLocation = $FileLocation;
    }

    /**
     * Getter for format result
     *
     * @return mixed
     */
    public function getResult()
    {
        return $this->Result;
    }

    /**
     * Setter for format result
     *
     * @param $Results
     * @return mixed
     */
    public function setResult($Results)
    {
        return $this->Result = $Results;
    }
}