<?php
/**
 * User: venpan
 * Date: 09/11/2017
 * Time: 09:50
 */

namespace MakeResultClass\ToYAML;

include_once('ResultBase.php');
use ResultBase\ResultBase;
use Symfony\Component\Yaml\Yaml;
use Exception;

class ToYAML extends ResultBase
{
    /**
     * helper function to make YAML data file
     * outputs in DataTransformer::TARGET_DIR
     *
     * @param $ObjectData
     */
    public function ToYAML ($ObjectData)
    {
        // make array if required
        $Array = $this->prepareDataToWrite($ObjectData);

        try {
            // goodness of symfony
            $yaml = Yaml::dump($Array);
            file_put_contents($this->getFileLocation(), $yaml);
            $status = true;
        } catch (Exception $e) {
            echo 'Exception: ',  $e->getMessage(), "\n";
            $status = false;
        }

        $this->setResult($status);
    }

    /**
     * ToYAML constructor.
     */
    public function __construct()
    {

    }
}