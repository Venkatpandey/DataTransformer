<?php
/**
 * User: venpan
 * Date: 17/11/2017
 * Time: 14:01
 */
namespace MakeResultClass\ToCSV;

include_once('ResultBase.php');
use ResultBase\ResultBase;

header('Content-type: text/plain');

/**
 * Class ToCSV
 *
 * @package MakeResultClass\ToCSV
 */
class ToCSV extends ResultBase
{
    /**
     * Function to make CSV insert file
     * outputs in DataTransformer::TARGET_DIR
     *
     * @param $ObjectData
     */
    public function ToCSV ($ObjectData)
    {
        // lets make data as array and prepare it for writting
        $ArrayData = $this->prepareDataToWrite($ObjectData);

        $headerCSV = null;
        $createFileCSV = fopen($this->getFileLocation(),"w+"); // open file
        foreach ($ArrayData as $rowData) {
            if(!$headerCSV) {
                fputcsv($createFileCSV,array_keys($rowData));
                fputcsv($createFileCSV, $rowData);   // add the first row of data
                $headerCSV = true;
            } else {
                fputcsv($createFileCSV, $rowData);
            }
        }

        // fclose returns true on success
        $this->setResult(fclose($createFileCSV));
    }

    /**
     * ToCSV constructor.
     */
    public function __construct()
    {

    }
}