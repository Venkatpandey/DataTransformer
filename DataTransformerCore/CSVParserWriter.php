<?php
/**
 * User: venpan
 * Date: 14/10/2017
 * Time: 09:14
 */

namespace DataTransform\CSVParserWriter;
use DataTransform\MultiSort;
use DataTransform\MasterValidator;
use DataTransformer\MakeResultClass;
use DataTransform\DataObjects\DataObjects;
header('Content-type: text/plain');
include_once('MultiSort.php');
include_once ('ParserWriter/MakeResult.php');
include_once('Data/DataObjects.php');
//error_reporting(E_ERROR | E_PARSE);

/**
 * Class CSVParserWriter
 * Responsible for preparing data as per user request like sorting and validation
 *
 * @package DataTransform\CSVParserWriter
 */
class CSVParserWriter
{
    /**
     * Holder variable for Final processed Array
     *
     * @var
     */
    private $finalArrayData;

    /**
     * Holder vatiable to store final result
     *
     * @var
     */
    private $finalResult;

    /**
     * Getter function for Final processed Array
     *
     * @return mixed
     */
    public function getFinalArrayData()
    {
        return $this->finalArrayData;
    }

    /**
     * Setter function for Final processed Array
     *
     * @param mixed $finalArrayData
     */
    public function setFinalArrayData(array $finalArrayData)
    {
        $this->finalArrayData = $finalArrayData;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->finalResult;
    }

    /**
     * @param mixed $finalResult
     */
    public function setResult($finalResult)
    {
        $this->finalResult = $finalResult;
    }

    /**
     * Main action method to initiate all sorting and validation
     *
     * @param $ArrayData
     * @param $configData
     * @return bool
     */
    public function ParserWriterAction($ArrayData, $configData)
    {
        $ObjectData = DataObjects::Instance()->makeArrayToObject($ArrayData);

        //proceed with validation now if user requests
        if (isset($configData['Validation']['validation']) && '1' == $configData['Validation']['validation']) {
            // validate and sort
            $this->Validate($ObjectData, $configData);
        } else {
            // just sort
            $this->Sort($ObjectData, $configData);
        }
        // all requirements met, now we can proceed to prepare files
        $Status = $this->makeResult($this->getFinalArrayData(), $configData);

        return $Status;
    }

    /**
     * @param $ObjectData
     * @param $Format
     * @return mixed
     */
    private function makeResult($ObjectData, $Format)
    {
        // initiate make result and deliver process
        $ResultData = new MakeResultClass\MakeResultClass($ObjectData, $Format);

        return $ResultData->getResultStatus();
    }

    /**
     * @param $ObjectData
     * @param $Post
     */
    private function Validate($ObjectData, $Post)
    {
        // initiate validate class, validate and proceed with sorting
        $Validated = new MasterValidator\MasterValidator($ObjectData, $Post['Validation']);
        $ValidatedData = $Validated->getValidatedData();
        $this->Sort($ValidatedData, $Post);
    }

    /**
     * Mini function to sort array
     *
     * @param $ObjectData
     * @param $Post
     */
    private function Sort ($ObjectData, $Post)
    {
        $Sorted= new MultiSort\MultiSort($ObjectData, $Post);
        $SortedData = $Sorted->getSortedData();
        $this->setFinalArrayData($SortedData);
    }

    /**
     * CSVParserWriter constructor.
     *
     * @param $arrayData
     * @param $configData
     */
    function __construct($arrayData, $configData)
    {
        $Status = $this->ParserWriterAction($arrayData, $configData);
        $this->setResult($Status);
    }
}