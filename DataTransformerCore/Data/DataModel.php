<?php
/**
 * User: venpan
 * Date: 17/11/2017
 * Time: 18:29
 */

namespace DataTransform\DataObjects;

/**
 * Class DataModel
 *
 * @package DataTransform\DataObjects
 */
class DataModel
{
    /**
     * Variable to hold model-data map
     *
     * @var
     */
    private $ModelMapping;

    /**
     * Holder for Model array
     *
     * @var
     */
    private $ModelArray;

    /**
     * Getter function for Model-data map
     *
     * @return DataModel
     */
    public function getModelMapping()
    {
        return $this->ModelMapping;
    }

    /**
     * Getter for model array
     *
     * @return DataModel
     */
    public function getModelArray()
    {
        return $this->ModelArray;
    }

    /**
     * Function to process and map single element of array list data
     *
     * @param $Array
     * @return \DataTransform\DataObjects\DataModel
     */
    public function processAndMap ($Array)
    {
        // basic init
        $Idx = 0;
        $ModelArray = [];
        $ModelMapping = [];

        // loop throgh, replace incoming key with NodeIdx and save incomingKey => NodeIdx as model for maping
        if (is_array($Array)) {
            foreach ($Array as $Key => $Val) {
               $ModelArray['Node' . $Idx] = $Val;
               $ModelMapping[$Key] = 'Node' . $Idx;
                $Idx++;
            }
        }
        $this->setDataModel($ModelArray, $ModelMapping);

        return DataModel::Instance();
    }

    /**
     * Setters for modeldata, and modelmap
     *
     * @param $ModelArray
     * @param $ModelMapping
     */
    private function setDataModel($ModelArray, $ModelMapping)
    {
        $this->ModelArray = $ModelArray;
        $this->ModelMapping = $ModelMapping;
    }

    /**
     * Call this method to get singleton
     *
     * @return DataModel
     */
    public static function Instance()
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new DataModel();
        }
        return $instance;
    }

    /**
     * DataModel constructor.
     *
     */
    private function __construct ()
    {

    }
}
