<?php
/**
 * User: venpan
 * Date: 28/10/2017
 * Time: 12:19
 */

namespace DataTransform\DataObjects;
use ArrayObject;

include_once ('DataModel.php');
header('Content-type: text/plain');
/**
 * Class DataObjects
 *
 * @package DataTransform\DataObjects
 */
class DataObjects
{
    /**
     * holder for model map
     *
     * @array
     */
    private $ModelMapping;

    /**
     * Variable to hold Array data
     *
    * @var
    */
    private $ArrayData;

    /**
     * Variable to hold Object data
     *
    * @var
    */
    private $ObjectData;

    /**
     * Getter function for Array data
     *
    * @return mixed
    */
    public function getArrayOfData()
    {
        return $this->ArrayData;
    }

    /**
     * Setter function for Array data
     *
    * @param mixed $ArrayData
    */
    public function setArrayOfData(array $ArrayData)
    {
        $this->ArrayData = $ArrayData;
    }

    /**
     * Getter function for model map
     *
     * @return array
     */
    public function getModelMapping()
    {
        return $this->ModelMapping;
    }

    /**
     * Setter function for model map
     *
     * @param array $ModelMapping
     */
    public function setModelMapping($ModelMapping)
    {
        $this->ModelMapping = $ModelMapping;
    }

    /**
     * Getter function for object data
     *
    * @return mixed
    */
    public function getObjectData()
    {
        return $this->ObjectData;
    }

    /**
     * Setter function for object data
     *
    * @param mixed $ObjectData
    */
    public function setObjectData($ObjectData)
    {
        $this->ObjectData = $ObjectData;
    }

    /**
     * creates ArrayObject for passed array
     *
     * @param $ArrayData
     * @return \ArrayObject
     */
    private function makeObject($ArrayData)
    {
        // basic sanitization for array
        $ArrayData = $this->sanitiseArray($ArrayData);
        $DataModel = DataModel::Instance()->processAndMap($ArrayData);
        $this->setModelMapping($DataModel->getModelMapping());
        $ObjectData = new ArrayObject($DataModel->getModelArray(), ArrayObject::ARRAY_AS_PROPS);

        return $ObjectData;
    }

    /**
     * Simple function to remove any element if its key is not present
     *
     * @param $ArrayData
     * @return mixed
     */
    private function sanitiseArray($ArrayData)
    {
        foreach ($ArrayData as $key => $value)
        {
            if ($key == "") // bad row
            {
                unset($ArrayData[$key]);
            }
        }

        return $ArrayData;
    }

    /**
     * Creates list of object data
     *
     * @param $ArrayData
     * @return array
     */
    public function makeArrayToObject($ArrayData)
    {
        $ArrayOfObj = [];
        $ObjectModel = [];
        foreach ( $ArrayData as $entityId => $entity) {
            $ObjectData = $this->makeObject($entity);
            $ArrayOfObj[$entityId] = $ObjectData;
        }
        $ObjectModel['EntityData'] = $ArrayOfObj;
        $ObjectModel['EntityModel'] = $this->getModelMapping();

        return $ObjectModel;
    }

    /**
     * Converts list of Array objects to plain arrays
     *
     * @param $ArrayData
     * @return array
     */
    public function makeObjectToArray ($ArrayData)
    {
        $ArrayOfObj = [];
        foreach ($ArrayData as $entityId => $entity) {
            $ArrayData = (array) $entity;
            $ArrayOfObj[$entityId] = $ArrayData;
        }

        return $ArrayOfObj;
    }

    /**
     * Function to prepare final data for writing to files,
     * makes object to array, appaend original key back from model
     *
     * @param $Data
     * @return array
     */
    public function prepareDataToWrite($Data)
    {
        $Arraydata = $this->makeObjectToArray($Data['EntityData']);
        $Model = $Data['EntityModel'];
        $Arraydata_r = []; // array to be returned
        foreach ($Arraydata as $entityId => $entity) {
            $Arraydata_r[$entityId] = $this->fetchArray($entity, $Model);
        }

        return $Arraydata_r;
    }

    /**
     * Helper function for prepareDataToWrite
     * @see prepareDataToWrite()
     *
     * @param $entity
     * @param $Model
     * @return array
     */
    private function fetchArray($entity, $Model)
    {
        $entity_r = []; // array to be returned
        foreach ($entity as $node => $value){
            $entity_r[array_search($node, $Model)] = $value;
        }

        return $entity_r;
    }

    /**
     * Call this method to get singleton
     *
     * @return DataObjects
     */
    public static function Instance()
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new DataObjects();
        }
        return $instance;
    }

    /**
     * DataObjects constructor.
     *
     */
    private function __construct()
    {
        // constructor
    }
}