<?php
/**
 * @author: Venkat Raman Pandey
 * Date: 12/10/2017
 * Time: 10:06
 */

namespace DataTransform\MultiSort;
use Arrch\Arrch;
use DataTransform\ArrayMaker\ArrayMaker;

include_once('MasterValidator.php');
include_once ("/var/www/devTest/lib/Arrch.php");
header('Content-type: text/plain');

class MultiSort
{
    /**
     * Holder variable for sorted array
     *
     * @var
     */
    private $sortedData;

    /**
     * Getter function for sorted array
     *
     * @return mixed
     */
    public function getSortedData()
    {
        return $this->sortedData;
    }

    /**
     * Setter function for sorted array
     *
     * @param mixed $sortedData
     */
    public function setSortedData($sortedData)
    {
        $this->sortedData = $sortedData;
    }

    /**
     * Sorting direction mapper array
     *
     * Tip:- can be added to html directly as value
     * @var array
     */
     private static $sortMapper = [
        1 => 'ASC',
        2 => 'DESC',
     ];

    /**
     * Function to sort array as per user request
     *
     * @param $Data
     * @param $sortType
     * @return \Arrch\arr $resultData
     * @TODO: find better way to compare pre and post sort data.
     */
    private function sortArray ($Data, $sortType)
    {
        $Model = $Data['EntityModel'];
        $sortOrder = (int) $sortType[0]; // sort order
        $sortThis = $Model[$sortType[1]]; // sort column
        $sortingMethod = self::$sortMapper[$sortOrder];

        $dataUnSot = $Data['EntityData']; // to compare later

        // sort data using Arrch library
        $resultData = Arrch::sort($Data['EntityData'], $sortThis, $sortingMethod);
        //$resultData = DataObjects::Instance()->makeArrayToObject($resultArray);
//        $arraysAreEqual = Arrch::compare([
//            $resultData, '==', $dataUnSot
//                        ]);

        return $resultData;
    }

    /**
     * MultiSort constructor.
     *
     * @param $Data
     * @param $postSort
     */
    function __construct($Data, $postSort)
    {
        if($postSort['download'] == '1') {
            $sorterType = explode(', ', $postSort["sort"]);
        } else {
            $sorterType = [
                0 => $postSort['Sorting']['order'],
                1 => $postSort['Sorting']['sort'],
            ];
        }
        $sortedArrayData = $this->sortArray($Data, $sorterType);
        $Data_r = ArrayMaker::Maker()->CombineModelData($Data, $sortedArrayData);
        $this->setSortedData($Data_r);
    }
}