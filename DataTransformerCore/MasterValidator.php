<?php
/**
 * @author: Venkat Raman Pandey
 * Date: 12/10/2017
 * Time: 17:24
 */

namespace DataTransform\MasterValidator;
use DataTransform\ArrayMaker\ArrayMaker;
use Rakit\Validation\Validator;
header('Content-type: text/plain');
/**
 * Class MasterValidator
 * Responsible for All data validations
 *
 * @package MasterValidator
 */
class MasterValidator
{
    /**
     * Holder variable for Validated Array
     *
     * @var
     */
    private $validatedData;

    /**
     * Getter function for Validated Array
     *
     * @return mixed
     */
    public function getValidatedData()
    {
        return $this->validatedData;
    }

    /**
     * Setter function for Validated Array
     *
     * @param mixed $validatedData
     */
    public function setValidatedData($validatedData)
    {
        $this->validatedData = $validatedData;
    }

    /**
     * MasterValidator constructor.
     *
     * @param $Data
     * @param $Conditions
     */
    function __construct($Data, $Conditions)
    {
        $validatedData = $this->validateData($Data, $Conditions);
        $Data_r = ArrayMaker::Maker()->CombineModelData($Data, $validatedData);
        $this->setValidatedData($Data_r);
    }

    /**
     * Function to validate Data using multiple validation rules
     *
     * @param \ArrayObject $Data
     * @param              $Conditions
     * @return \ArrayObject
     */
    private function validateData($Data, $Conditions)
    {
        $Model = $Data['EntityModel'];

        $badData = [];
        $validator = new Validator;
        foreach ($Data['EntityData'] as $dataId => $data) {
            //make validation
            $vali = $this->makeCondition($Conditions, $Model);
            $charValidation = $validator->make((array)$data, $vali);

            // then validate
            $charValidation->validate();

            if ($charValidation->fails()) {
                // handling errors
                $errors = $charValidation->errors();
                $badData[$dataId] = $errors->firstOfAll();
                unset($Data['EntityData'][$dataId]);
            }
        }

        return $Data['EntityData'];
    }

    /**
     * Helper function to arrange validation conditions as per user request
     *
     * @param $Conditions
     * @param $Model
     * @return array
     */
    private function makeCondition($Conditions, $Model)
    {
        $Validator = [];

        foreach ($Conditions as $col => $validationType) {
            if ($validationType == trim($validationType) && strpos($validationType, ' ') != false) {
                $validation_Type = $this->typeSeparator($validationType);
                $Validator[$Model[$col]] = $validation_Type;
            } elseif ($col !== "validation" && isset($Model[$col])) {
                $Validator[$Model[$col]] = $validationType;
            }
        }

        return $Validator;
    }

    /**
     * Helper function to separate valuidation sub-types
     *
     * @param $validationType
     * @return string
     */
    private function typeSeparator($validationType)
    {
        $validationType_r = '';
        $validation = explode(' ', $validationType);
        $validationType_r .= $validation[0].'|'.$validation[1];

        return $validationType_r;
    }
}