<?php
/**
 * @author: Venkat Raman Pandey
 * Date: 11/10/2017
 * Time: 11:02
 */

namespace DataTransform;
use DataTransform\ArrayMaker\ArrayMaker;
use DataTransform\CSVParserWriter;
header('Content-type: text/plain');
include_once('DataTransformerCore/CSVParserWriter.php');
include_once ('DataTransformerCore/ArrayMaker.php');
//error_reporting(E_ERROR | E_PARSE);

/**
 * Class DataTransformer
 *
 * @package DataTransform
 */
class DataTransformer
{
    // upload and result local directory
    const LOCAL_DIR = "uploadAndResults/";

    // some variable definition
    public $result;
    private $sourceFilename;

    /**
     * transformAction method to take care of file upload and CSV processing
     *
     * @param $post
     * @param $file
     * @return bool
     */
    public function transformAction($post, $file)
    {
        $this->sourceFilename = $file["upload"]["name"];
        $_SESSION['basename'] = basename($file["upload"]["name"], ".csv");
        $uploadOk = true; // optimistic :)

        // Check if csv file is a valid csv file indeed
        if (isset($post['submit'])) {
            $allowedsMimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv');
            if (in_array($file['upload']['type'], $allowedsMimes)) {
                $uploadOk = true;
            } else {
                $uploadOk = false;
            }
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == false) {
            $status = $this->processData($post, $uploadOk);
            // if everything is ok, try to upload file
        } else {
            // get it locally for processing
            if (move_uploaded_file($file['upload']['tmp_name'], self::LOCAL_DIR . $this->sourceFilename)) {
                $status = $this->processData($post, $uploadOk);
            } else {
                $status = $this->processData($post, $uploadOk);
            }
        }

        return $status;
    }

    /**
     * This method gets the array and process it for desired results
     *
     * @param $post
     * @param $bool
     * @return mixed
     * @TODO update form and this method for web ready
     */
    private function processData($post, $bool)
    {
        $ResultStatus = '';
        $post['download'] = "1"; // this is from web interface
        if ($bool) {
            $ArrayData = $this->makeArray($this->sourceFilename);
            $ParserWriter = new CSVParserWriter\CSVParserWriter($ArrayData, $post);
        } else {
            // something went wrong during upload :(
            $ResultStatus = false;
        }

        // proceed to finish
        if (null != $ParserWriter) {
            // should be true for success
            $ResultStatus = $ParserWriter->getResult();
        } else {
            // something went wrong :(
            $ResultStatus = false;
        }

        return $ResultStatus;
    }

    /**
     * bypass upload form-make use of configuration file
     *
     * @return mixed
     */
    public function processDataDirect()
    {
        $fileinfo = $this->getFileInfo();
        if ($fileinfo != null && $fileinfo['fileinfo'] != null) {
            $ArrayData = $this->makeArray($fileinfo['fileinfo']);
            $ParserWriter = new CSVParserWriter\CSVParserWriter($ArrayData, $fileinfo);
        } else {
            // something went wrong during retriving file infos :(
            $ResultStatus = false;
        }

        // proceed to finish
        if (null != $ParserWriter) {
            // should be true for success
            $ResultStatus = $ParserWriter->getResult();
        } else {
            // too bad
            $ResultStatus = false;
        }

        return $ResultStatus;
    }

    /**
     * Creates an array from file and config file for direct processing.
     *
     * @return mixed
     */
    private function getFileInfo()
    {
        $infoConfigData = parse_ini_file('configuration.ini', true);
        // Open a directory, and read its contents
        $file = scandir(self::LOCAL_DIR, 1);
        $infoConfigData['fileinfo'] = $file[0];
        $infoConfigData['basename'] = basename($file[0], pathinfo($infoConfigData['fileinfo'], PATHINFO_EXTENSION));

        return $infoConfigData;
    }

    private function explodeMulti ($Delimete, $Header)
    {
        $Prepare = str_replace($Delimete, $Delimete[0], $Header);
        $Explode = explode($Delimete[0], $Prepare);

        return $Explode;
    }

    /**
     * Helper function to make array out of provided file path
     *
     * @param $file
     * @return array
     */
    private function makeArray($file)
    {
        $FromExt = pathinfo($file, PATHINFO_EXTENSION);

        return ArrayMaker::Maker()->{strtoupper($FromExt)."ToArray"}(self::LOCAL_DIR . $file);
    }

    /**
     * DataTransformer constructor.
     *
     * @param $post
     * @param $file
     */
    function __construct($post = null, $file = null)
    {
        if ($post == null && $file == null) {
            $status = $this->processDataDirect();
        } else {
            $status = $this->transformAction($post, $file);
        }
        $this->result = $status;
    }
}
