<?php

/**
 * User: venpan
 * Date: 04/11/2017
 * Time: 15:47
 */

require_once 'vendor/autoload.php';
use DataTransform\DataTransformer;
header('Content-type: text/plain');
include_once ("uploadAndProcess.php");

if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == 'initiate.php') {
    // delete temp files
    array_map('unlink', glob("uploadAndResults/*"));
    $bool = new DataTransformer($_POST, $_FILES);
    // this needs some improvements :( lets settle for this hack for now
    $sbool = $bool->result;
    header("Location: index.php?action=".$sbool);
} else {
    // clear all file except csv
    foreach (glob("uploadAndResults/*") as $file) {
        $fileExtArr = explode('.', $file);
        $fileExt = $fileExtArr[count($fileExtArr)-1];
//        if($fileExt != "xml") {
//            unlink($file);
//        }
    }
    // start new
    $rustart = getrusage();
    $bool = new DataTransformer($_POST, $_FILES);
    function rutime($ru, $rus, $index) {
        return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
            -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
    }

    $ru = getrusage();
    // simple error handling
    if($bool->result == true) {
        echo "Success..! check uploadAndResults/ for results. \n";
        echo "This process used " . (rutime($ru, $rustart, "utime")) / 1000 .
            " s for its computations\n";
        echo "It spent " . (rutime($ru, $rustart, "stime")) / 1000 .
            " s in system calls\n";
    } else {
        echo "Not Success..! \n";
    }
    exit();
}
